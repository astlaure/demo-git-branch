Commands

1. Create a branche
```bash
git checkout -b feature/console
```

1. stage all the changes
```bash
git add .
```

1. commit everything
```bash
git commit -m "I did it"
```

1. push the new branch to remote (only the first time)
```bash
git push --set-upstream origin feature/console
```

1. push the branch after the first time
```bash
git push
```

1. do the merge request in GitLab